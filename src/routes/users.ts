import express from 'express';
import morgan from 'morgan';
import log from '@ajar/marker';

const router = express.Router();

const searchLogger = (req: express.Request, res: express.Response, next: express.NextFunction)=> {
    log.info(`${req.method} log from the search function level req.url:  ${ req.url }`)
    next()
}

// router.use( logger );
router.use(express.json());

router.get('/', (req, res) => {
    res.status(200).send('Get all Users')
});

//http://localhost:3030/users/search?food=pizza&place=kfar-saba
//req.query - access the querystring part of the request url
router.get('/search',searchLogger ,(req, res) => {
    log.blue(`query string food: ${req.query.food}`);
    log.blue(`query string place: ${req.query.place}`);
    res.status(200).json(req.query);
});

//http://localhost:3030/users/2345/Gal
//req.params - access dynamic parts of the url
router.get('/:userID/:userName',(req,res)=>{
    log.blue(`User ID: ${req.params.userID}`);
    log.blue(`User Name: ${req.params.userName}`);
    res.status(200).send(`User ID: ${req.params.userID}, User Name: ${req.params.userName}`);
});

//http://localhost:3030/users/shows
//req.body - access the request body of a POST request
router.post('/shows',(req,res)=> {
    log.info(req.body["first name"],req.body["last name"]);
    log.obj(req.body,'req.body is:');
    res.status(200).send(`Creating a new Show by the name of: ${req.body["first name"]}.`)
}).get("/shows", (req,res)=> {
    res.status(200).send("shows get req");
})

export default router;