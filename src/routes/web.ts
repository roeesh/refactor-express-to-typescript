import express from 'express';
import morgan from 'morgan';
import log from '@ajar/marker';

const router = express.Router();


// const logger = (req,res,next)=> {
//     log.info(`${req.method} log from the web-router req.url:  ${ req.url }`)
//     next()
// }

// router.use( logger );
// router.use( morgan('dev') );
router.use(express.json());


router.get('/', (req, res) => {
    res.status(200).send('Hello Express!')
});

//http://localhost:3030/animals
//return api json response
router.get('/animals',(req,res)=>{

    const data = [
        {animal1:'Lion',place:'asia'},
        {animal2:'Tiger',place:'Africa'}
    ]

    res.status(200).json(data);
    // res.status(200).send( JSON.stringify(data) )
});

//http://localhost:3030/hello/Eliran
//return html markup response
router.get('/hello/:name',(req,res)=>{

    // res.set('Content-Type','text/html');

    let data = 'Original Data';
    
    const markup = `<h1>Hello ${req.params.name}</h1>
                    <p>Get all of the following data:</p>
                    <ul>
                        <li>Hisory!</li>
                        <li>${data}</li>  
                    </ul>`
    res.status(200).set('Content-Type', 'text/html').send(markup)
    // res.status(200).send(markup)
});

export default router;