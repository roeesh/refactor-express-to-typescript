import {Router} from 'express';
import morgan from 'morgan';
import log from '@ajar/marker';


const router = Router();

router.use( morgan('dev') );

router.get('/',  (req, res) => {
    res.status(200).send('Hello api!')
});

export default router;