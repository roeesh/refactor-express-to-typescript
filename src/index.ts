import express, { NextFunction, Request, RequestHandler, Response } from 'express';
import morgan from 'morgan';
import log from '@ajar/marker';

import apiRouter from './routes/api.js'
import webRouter from './routes/web.js';
import usersRouter from './routes/users.js';


const { PORT , HOST = "localhost" } = process.env;
// const PORT: number = 3030;
// const HOST: string = "localhost";

const app = express();

const logger = (req: Request,res: Response ,next: NextFunction)=> {
    log.info(`${req.method} log from app level req.url:  ${ req.url }`)
    next()
}
// const logger: RequestHandler = (req,res ,next)=> {
//     log.info(`${req.method} log from app level req.url:  ${ req.url }`)
//     next()
// }

app.use( logger );
// app.use( morgan('dev') );
app.use(express.json());

app.use('/', webRouter);
app.use('/api', apiRouter);
app.use('/users', usersRouter);

// default- return 404 status with a custom response to unsupported routes
app.use((req, res,next) => {
    const full_url = new URL(req.url, `http://${req.headers.host}`);
    res.status(404).send(` - 404 - url ${full_url.href} was not found`)
});

app.listen(Number(PORT), HOST,  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});


